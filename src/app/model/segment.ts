import { Point } from "./point"

  
export function findIntersectionAtY(y:number,p1:Point,p2:Point) {
    if (p1.y == p2.y) return NaN;
    let pt1 = p1, pt2 = p2;
    if (p1.y > p2.y) {
      pt2 = p1; pt1 = p2;
    }
    if (y<pt1.y) return NaN;
    if (y>pt2.y) return NaN;
    let t = (y - pt1.y)/(pt2.y - pt1.y)
    return pt1.x + t *(pt2.x - pt1.x);
}
  
export class Segment {
    p1:Point;
    p2:Point;
  
    constructor(p1:Point, p2:Point) {
        this.p1 = p1;
        this.p2 = p2;
    }
    
    /*stroke(context: CanvasRenderingContext2D, color:string) {
      context.beginPath();
      context.moveTo(this.p1.x,this.p1.y);
      context.lineTo(this.p2.x,this.p2.y);
      context.stroke();
    }*/
    minY():number {
      return Math.min(this.p1.y,this.p2.y);
    }
    maxY():number {
      return Math.max(this.p1.y,this.p2.y);
    }
  
    toString() { 	return `[` + this.p1.toString() + `,` + this.p2.toString() + `]`;  }

    getCenter() : Point {
        return new Point ((this.p1.x + this.p2.x)/2, (this.p1.y + this.p2.y )/2);
    }
    
  
  }