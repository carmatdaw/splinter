import { Component, OnInit, ViewChild, ElementRef } from '@angular/core'
import { MatSlider } from '@angular/material/slider'
import { MatSelect } from '@angular/material/select'
import { SplinterSettings } from './model/splintersettings'
import { ProgressMeter } from './model/progressMeter'

let White="#FFFFFF"
let Black="#000000"

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'splinter';
  opened:boolean = true;
  voronoiChecked:boolean = false;
  events: string[] = [];
  width: number = 500;
  height: number = 500;
  imgURL: any;
  image = new Image;
  worker: Worker = null;
  Regenerate = "Regenerate";
  selectedColorScheme: string = "black-white";
  selectedStockImage: string = "natalie-1024.jpg";
  isNewImage: boolean = true;
  progressMeter: ProgressMeter = new ProgressMeter();

  @ViewChild('myCanvas',{static:true}) targetCanvas: ElementRef;
  @ViewChild('sourceCanvas',{static:true}) sourceCanvas: ElementRef;
  @ViewChild('workingGif',{static:true}) workingGif: ElementRef;
  @ViewChild('chooseFileInput',{static:true}) chooseFileInput: ElementRef;
  @ViewChild('outputImageWidth', {static: false}) outputImageWidth: ElementRef;
  @ViewChild('cellScale', {static: false}) cellScale: MatSlider;
  @ViewChild('minCellSize', {static: false}) minCellSize: MatSlider;
  @ViewChild('lineThickness', {static: false}) lineThickness: MatSlider;
  @ViewChild('stockImages', {static: false}) stockImages: MatSelect;
  constructor() {

  }

  generateImage(){

    let sourceCanvas = document.createElement('canvas');
    sourceCanvas.width = this.image.width;
    sourceCanvas.height = this.image.height;
    let ratio = this.image.height / this.image.width;
    let potentialWidth = parseFloat(this.outputImageWidth.nativeElement.value);
    if (!isNaN(potentialWidth) && potentialWidth > 100 && potentialWidth < 10000) {
      this.width = potentialWidth;
    }
    this.height = this.width * ratio;
    this.targetCanvas.nativeElement.width = this.width;
    this.targetCanvas.nativeElement.height = this.height;

    let targetContext: CanvasRenderingContext2D =  this.targetCanvas.nativeElement.getContext('2d');
    if (this.voronoiChecked) {
      targetContext.lineWidth = (+this.lineThickness.value)/5;
    } else {
      targetContext.lineWidth = (+this.lineThickness.value)/10;
    }
    let invert = false;
    let foregroundColor = Black;
    let backgroundColor = White;
    switch(this.selectedColorScheme) {
      case "black-white": {
        break;
      }
      case "white-black": {
        invert = true;
        foregroundColor = White;
        backgroundColor = Black;
        break;
      }
      case "sepia": {
        foregroundColor = "#704214";
        backgroundColor = White;
        break;
      }
      case "sepia-inverse": {
        invert = true;
        foregroundColor = White;
        backgroundColor = "#704214";
        break;
      }
      case "cool-grey": {
        foregroundColor = "#000020";
        backgroundColor = "#E0E0FF";
        break;
      }
      case "cool-grey-inverse": {
        invert = true;
        foregroundColor = "#E0E0FF";
        backgroundColor = "#000020";
        break;
      }

    }
    let linearScale = this.cellScale.value + Math.log2(Math.floor(this.width / 500));
    let cellScaleAdjusted = Math.pow(2,linearScale);
    
    let settings = new SplinterSettings(sourceCanvas.width,sourceCanvas.height, this.width,this.height,10,this.lineThickness.value/5, cellScaleAdjusted ,this.minCellSize.value,invert,foregroundColor,backgroundColor,this.voronoiChecked);

    let sourceContext = sourceCanvas.getContext('2d');
    sourceContext.drawImage(this.image,0,0);
    let greyScaleSamples = this.computeSamplerGreyScaleData(sourceContext,sourceCanvas.width,sourceCanvas.height);
    // kick off the worker
    this.progressMeter.start();
    this.worker.postMessage({message:'makeItSo', settings: settings, newImage: this.isNewImage, greyScaleSamples: greyScaleSamples},[greyScaleSamples.buffer]);
    this.isNewImage = false;

    
  }

  fillCanvas(ctx,cWidth,cHeight,color:string) {
    ctx.fillStyle = color;
    ctx.fillRect(0,0,cWidth,cHeight);
  }

  Clamp(value: number, min: number, max: number) {
    let clamped = value < min ? min : value;
    clamped = clamped > max ? max : value;
    return clamped;
  }

  computeSamplerGreyScaleData(ctx: CanvasRenderingContext2D, width:number, height:number):Uint8Array {
    let pixCount = width * height;
    let outputData = new Uint8Array(pixCount);
    
    // convert to greyscale
    let index = 0;
    let imageData = ctx.getImageData(0,0,width,height);
    let data = imageData.data;

    for (let pix = 0;pix<pixCount; pix++)
    {
        let value = data[index++] * .3 + data[index++] * .59 + data[index++] * .11;
        outputData[pix] = value;
        index++; // skip the alpha
    }
    return outputData;
  }

  onRegenerateClicked() {
    this.generateImage(); 

  }

  onChoosePicClicked() {
    this.chooseFileInput.nativeElement.click();
  }

  onToggleSideNav() {
    this.opened = !this.opened;
  }

  onCloseSettings() {
    this.onToggleSideNav();
  }
  updateSourceCanvas() {
    this.isNewImage = true;
    let context = this.sourceCanvas.nativeElement.getContext('2d');
    
    context.fillStyle = White;
    context.fillRect(0,0,this.sourceCanvas.nativeElement.width,this.sourceCanvas.nativeElement.height);

    let imageWidth = this.image.width;
    let imageHeight = this.image.height;
    let displayWidth = this.sourceCanvas.nativeElement.width;
    let displayHeight = this.sourceCanvas.nativeElement.height;
    let destWidth = displayHeight;
    let destHeight = displayHeight;
    if (imageWidth > imageHeight) {
      destHeight = (imageHeight / imageWidth) * displayHeight;
      context.drawImage(this.image,0,0,imageWidth,imageHeight,0,0,destWidth,destHeight);
    }
    else {
      destWidth = (imageWidth / imageHeight) * displayWidth;
      context.drawImage(this.image,0,0,imageWidth,imageHeight,(displayWidth - destWidth)/2,0,destWidth,destHeight);

    }
  }

  onImageChange(event) {
    this.selectedStockImage = undefined;
    var reader = new FileReader();
    let imagePath = event.target.files;
    reader.readAsDataURL(imagePath[0]); 
    reader.onload = (_event) => { 
      this.imgURL = reader.result; 
      this.image.src = this.imgURL;
    }
  }

  onStockImagePicked(event) {
    this.image.src = "/assets/images/" + this.selectedStockImage;
  }

  handleWebWorkerMessage(data) {
    if (data.message == "status") {
      this.progressMeter.update(data.stage,data.count);
      let context = this.targetCanvas.nativeElement.getContext("2d");
      context.drawImage(this.progressMeter.getCanvas(),0,0);
    } else if ( data.message == "splinterImageRendered" || data.message == "voronoiImageRendered") {
      let context = this.targetCanvas.nativeElement.getContext("2d");
      context.drawImage(data.image,0,0);
    }
    else {
      console.log(`Got unknown message: ${data.message}`);
    }
  }

  ngOnInit() {
    let canvas = this.targetCanvas;
    canvas.nativeElement.width = this.width;
    canvas.nativeElement.height = this.height;

    if (typeof Worker !== 'undefined') {
      // Create a new
      this.worker = new Worker(`./webworker.worker`, { type: `module` });
      this.worker.onmessage = ({ data }) => {
        this.handleWebWorkerMessage(data);
      };
    } else {
      // Web Workers are not supported in this environment.
      // You should add a fallback so that your program still executes correctly.
    }


    this.image.onload = function () {
      this.updateSourceCanvas();
      this.generateImage();
    }.bind(this);
    this.image.src = "/assets/images/" + this.selectedStockImage;


  }
}
