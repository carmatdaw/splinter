import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'num-input-comp',
  templateUrl: './num-input.component.html',
  styleUrls: ['./num-input.component.css']
})
export class NumInputComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  getValue(): string {
    return "testing"; // return the real value
  }
}
