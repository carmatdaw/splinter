import { ElementRef } from '@angular/core'
import { Sampler } from "./sampler"
import { Point } from "./point"
import { Quad } from "./quad"

export class Splinter {

    quads = [];
    slantAmount = .3;
    minQuadArea = 30;
    sampler: Sampler;
    cellScale = 0;
  
    constructor(sampler:Sampler) {
      this.sampler = sampler;
    }

    /*DoTheVoronoiThang(canvas: ElementRef, color:string,bounds:Bounds) {
        let context = canvas.nativeElement.getContext('2d');
        context.strokeStyle=color;
        // make a list of quad "center points"
        let points: Point[] = [];
        this.quads.forEach( function strokeAQuad(q:Quad) {
          points.push(q.getCenter());
        }.bind(this) );

        
        RenderVoronoi(points,canvas,bounds,color);
    }*/

    findSplit(p1:Point, p2:Point, t:number) {
            let dx = p2.x - p1.x;
        let dy = p2.y - p1.y;
        return new Point(p1.x + (t * dx), p1.y + (t*dy));
    }
    
    splitQuad(q:Quad)
    {
        let points = [];
      
      let area = q.area();
      if (area < this.minQuadArea) {
        return [q];
      }
  
      let value = this.sampler.sampleQuad(q); // this gives us a greyscale number from 0 to 255 of how dark an area should be
      let luminance =  (value * value * value) / 50000 ; // since we're dealing with how dark a 2d area should be we'll square the value and dived by 255 to get a nice curve
  
      let scaledArea = area / this.cellScale;
  
      if (scaledArea < luminance) { 
          return [q];
      }
      let t1 = Math.random() * this.slantAmount + ((1-this.slantAmount)/2);
      let t2 = Math.random() * this.slantAmount + ((1-this.slantAmount)/2);
  
    let l1 = q.p1.distance(q.p2) + q.p3.distance(q.p4);
      let l2 = q.p2.distance(q.p3) + q.p4.distance(q.p1);
      
          if (l1 < l2) { // split the quad vertically
        let splitPoint1 = this.findSplit(q.p2,q.p3,t1);
        let splitPoint2 = this.findSplit(q.p4,q.p1,t2);
        let q1h:Quad = new Quad(q.p1,q.p2,splitPoint1,splitPoint2);
        let q2h:Quad = new Quad(splitPoint2,splitPoint1,q.p3,q.p4);
        return [q1h,q2h];
      } else { // split the quad horizonatally
        let splitPoint1 = this.findSplit(q.p1,q.p2,t1);
        let splitPoint2 = this.findSplit(q.p3,q.p4,t2);;
        let q1v:Quad = new Quad(q.p1,splitPoint1,splitPoint2,q.p4);
        let q2v:Quad = new Quad(splitPoint1,q.p2,q.p3,splitPoint2);
          return [q1v,q2v];
      }
    }
  
    splitAll()
    {
      let oldQuads = this.quads;
      this.quads = []
      
      oldQuads.forEach( function splitIt(q:Quad) {
        let newQuads = this.splitQuad(q);
        this.quads.push(newQuads[0]);
        if (newQuads.length > 1) {
            this.quads.push(newQuads[1]);
        }
      }.bind(this) );
    }

    splitRecursive(q:Quad) : boolean {

        let area = q.area();
        if (area < this.minQuadArea) {
            let value = this.sampler.sampleQuad(q); // this gives us a greyscale number from 0 to 255 of how dark an area should be
            let luminance =  (value * value * value) / 50000 ; // since we're dealing with how dark a 2d area should be we'll square the value and dived by 255 to get a nice curve
            let scaledArea = area / this.cellScale;

            if (scaledArea > luminance) {
                this.quads.push(q);
                return true;
            }
            return false;
        }
  
        let t1 = Math.random() * this.slantAmount + ((1-this.slantAmount)/2);
        let t2 = Math.random() * this.slantAmount + ((1-this.slantAmount)/2);
    
        let l1 = q.p1.distance(q.p2) + q.p3.distance(q.p4);
        let l2 = q.p2.distance(q.p3) + q.p4.distance(q.p1);
        
        let q1:Quad = undefined;
        let q2:Quad = undefined;

        if (l1 < l2) { // split the quad vertically
            let splitPoint1 = this.findSplit(q.p2,q.p3,t1);
            let splitPoint2 = this.findSplit(q.p4,q.p1,t2);
            q1 = new Quad(q.p1,q.p2,splitPoint1,splitPoint2);
            q2 = new Quad(splitPoint2,splitPoint1,q.p3,q.p4);
        } else {
            let splitPoint1 = this.findSplit(q.p1,q.p2,t1);
            let splitPoint2 = this.findSplit(q.p3,q.p4,t2);;
            q1 = new Quad(q.p1,splitPoint1,splitPoint2,q.p4);
            q2 = new Quad(splitPoint1,q.p2,q.p3,splitPoint2);
        }

        let addedq1 = this.splitRecursive(q1);
        let addedq2 = this.splitRecursive(q2);
        // if each split child returns true then both need to be added
        // if neither 
        if (addedq1 || addedq2) {
            if (addedq1 && ! addedq2) {
                this.quads.push(q2);
            }
            if (addedq2 && !addedq1) {
                this.quads.push(q1);
            }
            return true;
        } else {
            let value = this.sampler.sampleQuad(q); // this gives us a greyscale number from 0 to 255 of how dark an area should be
            let luminance =  (value * value * value) / 50000 ; // since we're dealing with how dark a 2d area should be we'll square the value and dived by 255 to get a nice curve
            let scaledArea = area / this.cellScale;

            if (scaledArea > luminance) {
                this.quads.push(q);
                return true;
            }
            return false;
        }
    }

    addQuad(q:Quad) {
        this.quads.push(q);
    }
    
  }