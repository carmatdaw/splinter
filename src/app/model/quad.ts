import { Point } from "./point"
import { Segment, findIntersectionAtY } from "./segment"
  
export class Quad {
    p1:Point;
    p2:Point;
    p3:Point;
    p4:Point;
  
      constructor(p1:Point, p2:Point, p3:Point, p4:Point) {
        this.p1 = p1;
      this.p2 = p2;
      this.p3 = p3;
      this.p4 = p4;
    }
    
    segments():Segment[] {
        return [new Segment(this.p1,this.p2),new Segment(this.p2,this.p3),new Segment(this.p3,this.p4),new Segment(this.p4,this.p1)];
    }

    area() {
      function triArea(side1length,side2length,side3length) {
        var p = (side1length+side2length+side3length)/2;
        var a = Math.sqrt(p*(p-side1length)*(p-side2length)*(p-side3length));
        return a;
      }  
        let s1Length = this.p1.distance(this.p2);
      let s2Length = this.p2.distance(this.p3);
      let bisectLength = this.p1.distance(this.p3);
      let s3Length = this.p3.distance(this.p4);
      let s4Length = this.p4.distance(this.p1);
      return triArea(s1Length,s2Length,bisectLength) + triArea(s3Length,s4Length,bisectLength);
    }
  
    scale(sx:number, sy:number) {
      return new Quad(
        new Point(this.p1.x*sx,this.p1.y*sy),
        new Point(this.p2.x*sx,this.p2.y*sy),
        new Point(this.p3.x*sx,this.p3.y*sy),
        new Point(this.p4.x*sx,this.p4.y*sy)    
      );
    }
    minY():number {
      return Math.min(this.p1.y,Math.min(this.p2.y,Math.min(this.p3.y,this.p4.y)));
    }
    maxY():number {
      return Math.max(this.p1.y,Math.max(this.p2.y,Math.max(this.p3.y,this.p4.y)));
    }
  
    xSpan(y:number) {
      let xIntersections= [];
      // find which edges if any intersect the given Y value
      let intersection = findIntersectionAtY(y,this.p1,this.p2);
      if (!Number.isNaN(intersection)) xIntersections.push(intersection);
      intersection = findIntersectionAtY(y,this.p2,this.p3);
      if (!Number.isNaN(intersection)) xIntersections.push(intersection);
      intersection = findIntersectionAtY(y,this.p3,this.p4);
      if (!Number.isNaN(intersection)) xIntersections.push(intersection);
      intersection = findIntersectionAtY(y,this.p4,this.p1);
      if (!Number.isNaN(intersection)) xIntersections.push(intersection);
      if (xIntersections.length < 1) return [];
      if (xIntersections.length < 2) return [xIntersections[0],xIntersections[0]];
      if (xIntersections[0] < xIntersections[1]) return [xIntersections[0],xIntersections[1]];
      return [xIntersections[1],xIntersections[0]];
    }
  
    toString() { 	return `[` + this.p1.toString() + `,` + this.p2.toString() + `,` + this.p3.toString() + `,` + this.p4.toString() + `]`;  }

    getCenter() : Point {
        return new Point ((this.p1.x + this.p2.x + this.p3.x + this.p4.x)/4, (this.p1.y + this.p2.y + this.p3.y + this.p4.y)/4);
    }
    
  
  }