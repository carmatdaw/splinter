import {Quad} from './quad'



export class Sampler {
    scaleX = 1;
    scaleY = 1;
  
    constructor(public greyscale:Uint8Array,public sourcewidth:number, public sourceheight:number, public targetwidth:number, public targetheight:number, public invert:boolean) {
      
        this.scaleX = this.sourcewidth /this.targetwidth;
        this.scaleY = this.sourceheight/this.targetheight;
    }
        
    sampleQuad(targetQuad:Quad):number {
      // need to bounds check against image
      // need to figure out Y range
      let sourceQuad = targetQuad.scale(this.scaleX, this.scaleY);
  
      let minY:number = sourceQuad.minY();
      let maxY:number =sourceQuad.maxY();
  
      let total = 0;
      let count = 0;
      // scan from top to bottom of Y range
      for (let y=minY; y<=maxY; y++) {
        let xSpan = sourceQuad.xSpan(y);
        let yRow = Math.floor(y);
        let xCol = Math.floor(xSpan[0]);
        if (xSpan.length == 1) { // this probably isn't necessary, but the span checking code theoritically could return one intersection so...
          let index = yRow*this.sourcewidth + xCol;
          total += this.greyscale[index];
          count++;
        }
        else if (xSpan.length == 2) { 
          let length = xSpan[1] - xSpan[0];
          if (length < 1) length = 1;
          let index = yRow*this.sourcewidth + xCol;
          
          // add up value of each pixel and also count number of pixels 
          while(length >0 ) {
            total += this.greyscale[index++];
            count++;
            length--;
          }
        }
        else {
          console.log("huh?");
        }
      }
      // return the average pixel value
      if (count == 0) {
        return 1;
      }
      let result = total/count;
      return this.invert?255-result:result;
    }
    
  }