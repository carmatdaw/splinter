export class Point {
    x:number;
    y:number;
    
    constructor(x:number, y:number) {
        this.x = x;
        this.y = y;
    }
    
    distance(other: Point) {
        let dx = other.x - this.x;
      let dy = other.y - this.y;
      return Math.sqrt( (dx * dx) + (dy * dy) );
    }
    
    toString() {	return `(` + this.x + `,` + this.y + `)`;  }
    
  }
