const progressMeterSize:number = 500;

//TODO: move to components directory... it wasn't being built properly from there...Don't know why

export class ProgressMeter {

    canvas:HTMLCanvasElement = null;
    lastUpdate:number = 0;
    text: string[] = [];
    lastMessage:string;

    constructor() {
        this.canvas = document.createElement('canvas');
        this.canvas.width = progressMeterSize;
        this.canvas.height = progressMeterSize;

    }

    start() {
        this.lastUpdate = performance.now();
        this.text = ["Working..."];
    }
    update(message:string, count:number) {
        let currentTime = performance.now();
        let elapsedTime = currentTime - this.lastUpdate;

        let formattedMessage:string = "";
        if (count >= 0) {
            formattedMessage = message + " : " + count;
        } else {
            formattedMessage = message;
        }
        if (this.lastMessage != message && this.lastMessage != undefined) {
            this.text.push(this.lastMessage);
        }
        let context = this.canvas.getContext("2d");
        context.fillStyle = "#FFFFFF";
        context.fillRect(0,0,progressMeterSize,progressMeterSize);

        context.fillStyle = "#000000";

        context.font = "20px Consolas"

        let yPos:number = 30;
        let yAdvance:number = 30;
        for (let i=0; i<this.text.length; i++) {
            context.fillText(this.text[i],10,yPos);
            yPos += yAdvance;
        }
        context.fillText(formattedMessage,10,yPos);
        this.lastMessage = message;
    }

    getCanvas() {return this.canvas;}
}