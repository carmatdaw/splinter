import { Point } from "./point"

export class Bounds {
    constructor(public low:Point, public high:Point) {}

    width():number {return this.high.x - this.low.x;}
    height():number {return this.high.y - this.low.y;}
    top():number {return this.low.y;}
    left():number {return this.low.x;}
    right():number {return this.high.x;}
    bottom():number {return this.high.y;}

}