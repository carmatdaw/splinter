export class SplinterSettings {
    constructor(public sourcewidth:number,
                public sourceheight:number,
                public targetwidth:number,
                public targetheight:number,
                public margin:number,
                public lineThickness:number,
                public cellScale:number,
                public minCellSize:number,
                public invertSampling:boolean,
                public foregroundColor:string,
                public backgroundColor:string,
                public voronoiChecked:boolean){
  
    }
  }
  