/*
  Thanks to  Nicolas Belmonte for his excellent Javascript
  implementation of the Voronoi algorithm which served as the 
  basis for this port to TypeScript.
  
  His original blog post and demo appear here:
  https://philogb.github.io/blog/2010/02/12/voronoi-tessellation/
*/

import { Point } from './point';
import { Quad } from './quad';
import { Segment } from './segment';

export class Voronoi {

    edges: Segment[] = [];
  
    top: number = 0;
    left: number = 0;

    edgeUpdateThreshold = 3773;

    //TODO: simplify this to just passing in the bounds
    pymin: number = 0;
    pymax: number = 0;
    pxmin: number = 0;
    pxmax: number = 0;
  
    constructor(public width:number, public height:number, public margin:number) {
        this.pymin = margin;
        this.pymax = height-margin;
        this.pxmin = margin;
        this.pxmax = width-margin;

    }
  
    computeVoronoi(points: Point[] ) {

        this.edgeUpdateThreshold = 1373;
        let sites = new Sites(points);
        let geom = new Geom(this);
        let edgeList = new EdgeList(sites,geom);
        let events = new EventQueue();
      
        let newSite:Point = sites.list.shift();
        let newIntStar:Point;
        let lbnd:Edge,rbnd:Edge,llbnd:Edge, rrbnd:Edge;
        let bisector:Edge;
        let bot:Point, top:Point;
        var temp:Point,p:Point,v:Point;
        let pm:string;
        let e:Edge;
        let count = 0;
        while(true) {
          if(!events.empty()) {
            newIntStar = events.min();
          }
          if(newSite && (events.empty() 
            || newSite.y < newIntStar.y
            || (newSite.y == newIntStar.y 
            && newSite.x < newIntStar.x))) { //new site is smallest
          
            lbnd = edgeList.leftBound(newSite);
            rbnd = edgeList.right(lbnd);
            bot = edgeList.rightRegion(lbnd);
            e = geom.bisect(bot, newSite);
            bisector = edgeList.createHalfEdge(e, "left");
            edgeList.insert(lbnd, bisector);
            p = geom.intersect(lbnd, bisector);
            if(p) {
              events.del(lbnd);
              events.insert(lbnd, p, geom.distance(p, newSite));
            }
            lbnd = bisector;
            bisector = edgeList.createHalfEdge(e, "right");
            edgeList.insert(lbnd, bisector);
            p = geom.intersect(bisector, rbnd);
            if(p) {
              events.insert(bisector, p, geom.distance(p, newSite));
            }
            newSite = sites.list.shift();
          } else if(!events.empty()) { //intersection is smallest
            lbnd = events.extractMin();
            llbnd = edgeList.left(lbnd);
            rbnd = edgeList.right(lbnd);
            rrbnd = edgeList.right(rbnd);
            bot = edgeList.leftRegion(lbnd);
            top = edgeList.rightRegion(rbnd);
            //target.plotTriple(bot, top, edgeList.rightRegion(lbnd));
            v = lbnd.vertex;
            geom.endPoint(lbnd.edge, lbnd.side, v);
            geom.endPoint(rbnd.edge, rbnd.side, v);
            edgeList.del(lbnd);
            events.del(rbnd);
            edgeList.del(rbnd);
            pm = "left";
            if(bot.y > top.y) {
              temp = bot;
              bot = top;
              top = temp;
              pm = "right";
            }
            e = geom.bisect(bot, top);
            bisector = edgeList.createHalfEdge(e, pm);
            edgeList.insert(llbnd, bisector);
            geom.endPoint(e, pm == "left"? "right":"left", v);
            p = geom.intersect(llbnd, bisector);
            if(p) {
              events.del(llbnd);
              events.insert(llbnd, p, geom.distance(p, bot));
            }
            p = geom.intersect(bisector, rrbnd);
            if(p) {
              events.insert(bisector, p, geom.distance(p, bot));
            }
          } else {
            break;
          }
        }
      
         for(lbnd = edgeList.right(edgeList.leftEnd);
            lbnd != edgeList.rightEnd;
            lbnd = edgeList.right(lbnd)) {
          e = lbnd.edge;
          this.clipLine(e);
        }
    }     
 
    clipLine(e:Edge):void {

      let s1:Point, s2:Point;
      let x1:number, x2:number, y1:number, y2:number;
      if(e.a == 1 && e.b >= 0) {
        s1 = e.ep.right;
        s2 = e.ep.left;
      } else {
        s1 = e.ep.left;
        s2 = e.ep.right;
      }
      if(e.a == 1) {
        y1 = this.pymin;
        if(s1 && s1.y > this.pymin) {
          y1 = s1.y;
        }
        if(y1 > this.pymax) {
          return;
        }
        x1 = e.c - e.b * y1;
        y2 = this.pymax;
        if(s2 && s2.y < this.pymax) {
          y2 = s2.y;
        }
        if(y2 < this.pymin) {
          return;
        }
        x2 = e.c - e.b * y2;
        if(((x1 > this.pxmax) && (x2 > this.pxmax)) || ((x1 < this.pxmin) && (x2 < this.pxmin))) {
          return;
        }
        if(x1 > this.pxmax) {
          x1 = this.pxmax;
          y1 = (e.c - x1) / e.b;
        }
        if(x1 < this.pxmin) {
          x1 = this.pxmin;
          y1 = (e.c - x1) / e.b;
        }
        if(x2 > this.pxmax) {
          x2 = this.pxmax;
          y2 = (e.c - x2) / e.b;
        }
        if(x2 < this.pxmin) {
          x2 = this.pxmin;
          y2 = (e.c - x2) / e.b;
        }
      } else {
        x1 = this.pxmin;
        if(s1 && s1.x > this.pxmin) {
          x1 = s1.x;
        }
        if(x1 > this.pxmax) {
          return;
        }
        y1 = e.c - e.a * x1;
        x2 = this.pxmax;
        if(s2 && s2.x < this.pxmax) {
          x2 = s2.x;
        }
        if(x2 < this.pxmin) {
          return;
        }
        y2 = e.c - e.a * x2;
        if(((y1 > this.pymax) && (y2 > this.pymax)) || ((y1 < this.pymin) && (y2 < this.pymin))) {
          return;
        }
        if(y1 > this.pymax) {
          y1 = this.pymax;
          x1 = (e.c - y1) / e.a;
        }
        if(y1 < this.pymin) {
          y1 = this.pymin;
          x1 = (e.c - y1) / e.a;
        }
        if(y2 > this.pymax) {
          y2 = this.pymax;
          x2 = (e.c - y2) / e.a;
        }
        if(y2 <this. pymin) {
          y2 = this.pymin;
          x2 = (e.c - y2) / e.a;
        }
     }
//TODO: consider simplifying the Segment class to just have x1,y1,x2,y2
// rather than two points
     this.edges.push(new Segment(new Point(x1,y1),new Point(x2,y2)));
     if (this.edges.length % this.edgeUpdateThreshold == 1) {
        postMessage({message: "status", stage:'Generating Voronoi Cels', count: this.edges.length },[]); 
        this.edgeUpdateThreshold = Math.floor(this.edgeUpdateThreshold * 1.05);
     }
    }
};
  
class Sites {
    bottomSite: Point;
    
    constructor(public list: Point[]) {
        this.sort();
        this.bottomSite = list.shift();
    }

    sort():void {
        this.list.sort(function(a, b) {
            if(a.y < b.y) { 
            return -1;
            } else if(a.y > b.y) {
            return 1;
            } else if(a.x < b.x) {
            return -1;
            } else if(a.x > b.x) {
            return 1;
            }
            return 0;
        });
    }
 
};

class Edge {
    public a: number = 0;
    public b: number = 0;
    public c: number = 0;
    public region:Span = null;
    public ep:Span = null;
    public edge:Edge = null;
    public side:string = "";
    public vertex:Point = null;
    public left:Edge = null;
    public right:Edge = null;
    public ystar:Number = 0;
}


class EdgeList {
    list: Edge[] = [];
    
    leftEnd: Edge = null;
    rightEnd: Edge = null;
  
    constructor(private sites:Sites, private geom:Geom) {
        
      // TODO: get rid of the strings for left and right--change to an enum
      this.leftEnd = this.createHalfEdge(null, "left");
      this.rightEnd = this.createHalfEdge(null, "left");
  
      this.leftEnd.right = this.rightEnd;
      this.rightEnd.left = this.leftEnd;
  
      this.list.unshift(this.leftEnd, this.rightEnd);
    }
    
    createHalfEdge(edge, side) : Edge {
      let e =  new Edge();
      e.edge = edge;
      e.side = side;
      return e;
    }
  
    insert(lb:Edge, he:Edge):void {
       he.left = lb;
       he.right = lb.right;
       lb.right.left = he;
       lb.right = he;
    }
  
    leftBound(p:Point):Edge {
      let he:Edge = this.leftEnd;
      do {
        he = he.right;
      } while (he != this.rightEnd && this.geom.rightOf(he, p)); 
      he = he.left;
      return he;
    }
  
    del(he:Edge):void {
      he.left.right = he.right;
      he.right.left = he.left;
      he.edge = null;
    }
  
    right(he):Edge {
      return he.right;
    }
  
    left(he):Edge {
      return he.left;
    }
  
    leftRegion(he):Point {
       if(he.edge == null) {
        return this.sites.bottomSite; 
       }
       return he.side == "left"?
        he.edge.region.left : he.edge.region.right;
    }
  
    rightRegion(he):Point {
      if(he.edge == null) {
        return this.sites.bottomSite;
      }
      return he.side == "left"?
        he.edge.region.right : he.edge.region.left;
    }
  };
  
  class Span {
    constructor(public left:Point, public right:Point) {}
    Get(which:string):Point { 
        if (which == "left" ) {
            return this.left
        } 
        return this.right;
    }
    Set(which:string, value:Point) {
        if(which == "left") {
            this.left = value;
        } else {
            this.right = value;
        }
    }
  }

  class Geom {
  
    constructor (private target:Voronoi) {
    }
  
    bisect(s1:Point, s2:Point): Edge {
        let newEdge = new Edge();
        newEdge.region = new Span(s1,s2);
        newEdge.ep = new Span(null,null);

        let dx = s2.x - s1.x,
            dy = s2.y - s1.y,
            adx = dx > 0? dx : -dx,
            ady = dy > 0? dy : -dy;
        
        newEdge.c = s1.x * dx + s1.y * dy 
            + (dx * dx + dy * dy) * 0.5;
    
        if(adx > ady) {
            newEdge.a = 1;
            newEdge.b = dy / dx;
            newEdge.c /= dx;
        } else {
            newEdge.b = 1;
            newEdge.a = dx / dy;
            newEdge.c /= dy;
        }
        return newEdge;
    }
  
    intersect (el1:Edge, el2:Edge): Point {
      var e1 = el1.edge, e2 = el2.edge;
      if(!e1 || !e2) {
        return null;
      }
      if(e1.region.right == e2.region.right) {
        return null;
      }
      var d = (e1.a * e2.b) - (e1.b * e2.a);
      if(Math.abs(d) < 1e-10) {
        return null;
      }
      let xint = (e1.c * e2.b - e2.c * e1.b) / d,
          yint = (e2.c * e1.a - e1.c * e2.a) / d;
      let e1r = e1.region.right,
          e2r = e2.region.right;
      let el:Edge, e:Edge;
      if((e1r.y < e2r.y) ||
         (e1r.y == e2r.y && e1r.x < e2r.x)) {
        el = el1;
        e = e1;
      } else {
        el = el2;
        e = e2;
      }
      var rightOfSite = (xint >= e.region.right.x);
      if((rightOfSite && (el.side == "left")) ||
        (!rightOfSite && (el.side == "right"))) {
        return null;
      }
      return new Point(xint,yint);
    }
  
    rightOf (he:Edge, p:Point):boolean {
      var e = he.edge,
          topsite = e.region.right,
          rightOfSite = (p.x > topsite.x);
  
      if(rightOfSite && (he.side == "left")) {
        return true;
      }
      if(!rightOfSite && (he.side == "right")) {
        return false;
      }
      let above:boolean = false;
      if(e.a == 1) {
        let dyp:number = p.y - topsite.y;
        let dxp:number = p.x - topsite.x;
        let fast:boolean = false;
  
        if((!rightOfSite && (e.b < 0)) ||
          (rightOfSite && (e.b >= 0))) {
          above = fast = (dyp >= e.b * dxp);
        } else {
          above = ((p.x + p.y * e.b) > e.c);
          if(e.b < 0) {
            above = !above;
          }
          if(!above) {
            fast = true;
          }
        }
        if(!fast) {
          var dxs = topsite.x - e.region.left.x;
          above = (e.b * (dxp * dxp - dyp * dyp)) < 
            (dxs * dyp * (1 + 2 * dxp / dxs + e.b * e.b));
  
          if(e.b < 0) {
            above = !above;
          }
        }
      } else /* e.b == 1 */ {
        var yl = e.c - e.a * p.x,
            t1 = p.y - yl,
            t2 = p.x - topsite.x,
            t3 = yl - topsite.y;
  
        above = (t1 * t1) > (t2 * t2 + t3 * t3);
      }
      return he.side == "left"? above : !above;
    }
  
    endPoint(edge:Edge, side:string, site:Point) {
      edge.ep.Set(side,site)
      var opSide = (side == "left"? "right":"left");
      if(!edge.ep.Get(opSide)) {
        return;
      }
      this.target.clipLine(edge);
    }
  
    distance(s:Point, t:Point):number {
      var dx = s.x - t.x,
          dy = s.y - t.y;
      return Math.sqrt(dx * dx + dy * dy);
    }
   };
  
  class EventQueue {
    list = [];
    
    constructor() {

    }

    insert(he:Edge, site:Point, offset:number):void {
      he.vertex = site;
      he.ystar = site.y + offset;
      for(var i=0, list=this.list, l=list.length; i<l; i++) {
        var next = list[i];
        if(he.ystar > next.ystar || 
          (he.ystar == next.ystar &&
          site.x > next.vertex.x)) {
          continue;
        } else {
          break;
        }
      }
      list.splice(i, 0, he);
    }
  
    del(he:Edge): void {
      for(var i=0, ls=this.list, l=ls.length; i<l && (ls[i] != he); ++i);
      ls.splice(i, 1);
    }
  
    empty():boolean { 
        return this.list.length == 0; 
    }
  
    nextEvent(he:Edge): Edge {
      for(var i=0, ls=this.list, l=ls.length; i<l; ++i) {
        if(ls[i] == he) return ls[i+1];
      }
      return null;
    }
  
    min():Point {
      var elem = this.list[0];
      return new Point( elem.vertex.x, elem.ystar )
    }
  
    extractMin() : Edge{
      return this.list.shift();
    }
  };

  