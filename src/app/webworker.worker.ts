/// <reference lib="webworker" />
import { SplinterWorker } from "./model/splinter.worker"
import { SplinterSettings } from "./model/splintersettings"
import { Sampler } from './model/sampler';
import { Quad } from './model/quad';
import { Point } from './model/point';

/*


*/
let splinterWorker = new SplinterWorker();
let sampler:Sampler = null;
let lastRenderedSettings = new SplinterSettings(0,0,0,0,0,0,0,0,false,"","",false);

addEventListener('message', ({ data }) => {

  switch(data.message) {
    case 'makeItSo':
      {
        let settings:SplinterSettings = data.settings;

        let mustRedoSplinter = data.newImage;

        if (settings.targetwidth != lastRenderedSettings.targetwidth ||
        settings.targetheight != lastRenderedSettings.targetheight ||
        settings.invertSampling != lastRenderedSettings.invertSampling || 
        settings.cellScale != lastRenderedSettings.cellScale ||
        settings.minCellSize != lastRenderedSettings.minCellSize) {
          mustRedoSplinter = true;
        }
        lastRenderedSettings = settings;

        if (mustRedoSplinter) {
          sampler = new Sampler(data.greyScaleSamples,settings.sourcewidth,settings.sourceheight,settings.targetwidth,settings.targetheight,settings.invertSampling);
          splinterWorker.setup(settings,sampler);
          splinterWorker.splinter();
        } else {
          splinterWorker.updateSettings(settings);
        }
        if (settings.voronoiChecked == false) {
            splinterWorker.renderSplinter();
        } else {
          splinterWorker.renderVoronoi();
        }
      }
    break;
    default:
      {
        console.log(`Web worker got unknown message ${data}`);
        const response = `worker response to ${data}`;
        postMessage(response);
      }
      break;
  }

});

