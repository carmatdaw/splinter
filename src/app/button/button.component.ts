import { Component, EventEmitter, OnInit, Input, Output } from '@angular/core';

@Component({
  selector: 'button-comp',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css']
})
export class ButtonComponent implements OnInit {

  constructor() { }
  @Input() text: string;
  @Output() onClicked = new EventEmitter<boolean>();
  ngOnInit() {
  }

  onClick() {
    console.log("Clicked");
    this.onClicked.emit(true);
  }

}
