import { Sampler } from "./sampler"
import { SplinterSettings } from "./splintersettings"
import { Point } from "./point"
import { Quad } from "./quad"
import { Segment } from "./segment"
import { Bounds } from "./bounds"
import { Voronoi } from './voronoi.worker'



export class SplinterWorker {

    quads:Quad[] = [];
    splinterEdges:Segment[] = [];
    voronoiEdges:Segment[] = []
    points:Point[] = []
    slantAmount = .2;
    minQuadArea = 30;
    sampler: Sampler = null;
    cellScale = 0;
    lastEdgeUpdate = 0;
    edgeUpdateThreshold = 13737;
    settings:SplinterSettings = null;

    setup(settings:SplinterSettings, sampler:Sampler) {
        this.settings = settings;
        this.sampler = sampler;
        this.minQuadArea = settings.minCellSize;
        this.cellScale = settings.cellScale;
        this.quads = [];
        this.points = [];
        this.voronoiEdges = [];
        this.lastEdgeUpdate = 0;
        let startQuad = new Quad(
            new Point(settings.margin,settings.margin),
            new Point(settings.targetwidth - settings.margin,settings.margin),
            new Point(settings.targetwidth - settings.margin, settings.targetheight-settings.margin),
            new Point(settings.margin, settings.targetheight - settings.margin));
        this.quads.push(startQuad);
        let startingEdges = startQuad.segments();
        this.splinterEdges = startingEdges;
    }
    updateSettings(settings:SplinterSettings) {
        this.settings = settings;
    }

    findSplit(p1:Point, p2:Point, t:number) {
        let dx = p2.x - p1.x;
        let dy = p2.y - p1.y;
        return new Point(p1.x + (t * dx), p1.y + (t*dy));
    }
    
    splitQuad(q:Quad)
    {
        let points = [];
      
        let area = q.area();
        if (area < this.minQuadArea) {
        return [q];
        }
  
        let value = this.sampler.sampleQuad(q); // this gives us a greyscale number from 0 to 255 of how dark an area should be
        let luminance =  (value * value * value) / 50000 ; // since we're dealing with how dark a 2d area should be we'll square the value and dived by 255 to get a nice curve

        let scaledArea = area / this.cellScale;

        if (scaledArea < luminance) { 
            return [q];
        }
        let t1 = Math.random() * this.slantAmount + ((1-this.slantAmount)/2);
        let t2 = Math.random() * this.slantAmount + ((1-this.slantAmount)/2);
    
            let l1 = q.p1.distance(q.p2) + q.p3.distance(q.p4);
        let l2 = q.p2.distance(q.p3) + q.p4.distance(q.p1);
        
            if (l1 < l2) { // split the quad vertically
            let splitPoint1 = this.findSplit(q.p2,q.p3,t1);
            let splitPoint2 = this.findSplit(q.p4,q.p1,t2);
            this.splinterEdges.push(new Segment(splitPoint1,splitPoint2));
            let q1h:Quad = new Quad(q.p1,q.p2,splitPoint1,splitPoint2);
            let q2h:Quad = new Quad(splitPoint2,splitPoint1,q.p3,q.p4);
            return [q1h,q2h];
        } else { // split the quad horizonatally
            let splitPoint1 = this.findSplit(q.p1,q.p2,t1);
            let splitPoint2 = this.findSplit(q.p3,q.p4,t2);;
            this.splinterEdges.push(new Segment(splitPoint1,splitPoint2));
            let q1v:Quad = new Quad(q.p1,splitPoint1,splitPoint2,q.p4);
            let q2v:Quad = new Quad(splitPoint1,q.p2,q.p3,splitPoint2);
            return [q1v,q2v];
        }
    }
  
    doOneSplitPass():boolean
    {
      let oldQuads = this.quads;
      this.quads = []
      
      oldQuads.forEach( function splitIt(q:Quad) {
        let newQuads = this.splitQuad(q);
        this.quads.push(newQuads[0]);
        if (newQuads.length > 1) {
            this.quads.push(newQuads[1]);
        }
        let edgesSinceUpdate = this.splinterEdges.length - this.lastEdgeUpdate;
        if (edgesSinceUpdate > this.edgeUpdateThreshold) {
            this.sendSplinterUpdate();
        }
      }.bind(this) );

      if (oldQuads.length == this.quads.length) {
          return false; // we did not create any new quads
      }
      return true; // we created new quads
    }

    splinter() {
        for (var i=0; i<24; i++) {
            if (!this.doOneSplitPass()){
                // we didn't split anything on the last pass, so we are done
                break; 
            }
        }

    }

    fillCanvas(ctx:OffscreenCanvasRenderingContext2D,width:number,height:number,color:string) {
        ctx.fillStyle = color;
        ctx.fillRect(0,0,width,height);
    }

    renderSplinter() {
        let canvas = this.renderEdges(this.splinterEdges);

        let bitmap = canvas.transferToImageBitmap();
        postMessage({message:"splinterImageRendered", image: bitmap},[bitmap]);

    }

    computeCenterPoints() {
        postMessage({message: "status", stage:"Computing Center Points", count: 0 });
        if (this.points.length == 0) {
            this.quads.forEach( function strokeAQuad(q:Quad) {
                this.points.push(q.getCenter());
            }.bind(this) );
        }
    }

    computeVoronoi() {
        if (this.quads.length == 0) {
            this.splinter();
        }
        if (this.voronoiEdges.length == 0) {
            this.computeCenterPoints();
            postMessage({message: "status", stage:"Computing Voronoi", count: 0});

            let voronoi:Voronoi = new Voronoi(this.settings.targetwidth, this.settings.targetheight, this.settings.margin);
            voronoi.computeVoronoi(this.points);
            this.voronoiEdges = voronoi.edges;
            postMessage({message: "status", stage:"Voronoi Done", count: 0});

        }
    }

    renderVoronoi() {
        this.computeVoronoi();

        let canvas = this.renderEdges(this.voronoiEdges);

        // draw border
        let context:OffscreenCanvasRenderingContext2D = <OffscreenCanvasRenderingContext2D>canvas.getContext("2d");
        context.beginPath();
        context.moveTo(this.settings.margin,this.settings.margin);
        context.lineTo(this.settings.targetwidth - this.settings.margin,this.settings.margin);
        context.lineTo(this.settings.targetwidth - this.settings.margin,this.settings.targetheight - this.settings.margin);
        context.lineTo(this.settings.margin,this.settings.targetheight - this.settings.margin);
        context.lineTo(this.settings.margin,this.settings.margin);
        context.stroke();

        //postMessage({message: "status", stage:"Rendering Image", count: -1});
        let bitmap = canvas.transferToImageBitmap();
        postMessage({message:"voronoiImageRendered", image: bitmap},[bitmap]);      
    }

    renderEdges(renderEdges: Segment[]):OffscreenCanvas {
        let canvas = new OffscreenCanvas(this.settings.targetwidth,this.settings.targetheight);
        let context:OffscreenCanvasRenderingContext2D = <OffscreenCanvasRenderingContext2D>canvas.getContext("2d");
        context.lineWidth = this.settings.lineThickness;
        context.strokeStyle = this.settings.foregroundColor;
        this.fillCanvas(context,this.settings.targetwidth,this.settings.targetheight,this.settings.backgroundColor); 

        let lastRenderingUpdate = 0;
        for (var i=0; i<renderEdges.length; i++) {
            let seg = renderEdges[i];
            context.beginPath();
            context.moveTo(seg.p1.x,seg.p1.y);
            context.lineTo(seg.p2.x,seg.p2.y);
            context.stroke();
            if (i - lastRenderingUpdate > this.edgeUpdateThreshold) {
                postMessage({message: "status", stage:"Rendering Edges", count: i});
                lastRenderingUpdate = i;
            }
        }
        return canvas
    }

    updateRenderSplinter(settings:SplinterSettings) {
        //TODO:  consider checking that nothing has changed that can't be dealt with
        // during a re-render
        // for now we're just going to re-render and the only things that will
        // be effectual are the render time settings - fore/back colors and line width
        this.settings = settings;
        this.renderSplinter();
    }

    updateRenderVoronoi(settings:SplinterSettings) {
        //TODO:  consider checking that nothing has changed that can't be dealt with
        // during a re-render
        // for now we're just going to re-render and the only things that will
        // be effectual are the render time settings - fore/back colors and line width
        this.settings = settings;
        this.renderVoronoi();
    }    

    splinterAndRender() {
        this.splinter();
        this.renderSplinter();
    }

       sendSplinterUpdate() {
        this.lastEdgeUpdate = this.splinterEdges.length;
        postMessage({message: "status", stage:'Splintering', count: this.splinterEdges.length }); 
    }


  }